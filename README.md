# TQS_Project1: Delivery Application

This project has three main sub-projects:

* A delivery platform that can be reused by different applications;
* A flutter-app for the riders where they can register themselves,inform whenever they finish a delivery and also be tracked by their location based on GPS signal;
* A food delivery web-application that will use this API to manage the riders that deliver it.

The first and second will allow the management of riders while the third will be built upon this delivery platform. Similarly to UberEats, the users will be able to order any food they want and get it delivered at their home by a rider.

## Resources
* [Documents Folder](https://drive.google.com/drive/folders/1Eyq8toTMicDV4ZJNXHhJlAIzj5vVtktx "Google Drive")
* [Project Backlog](https://projeto-informatica-pmate2021.atlassian.net/jira/software/projects/TQS/boards/2/backlog "Jira")
* [Itadakimasu Website](http://192.168.160.223:8080 "Itadakimasu API")
* [Vroom Website ](http://192.168.160.223:8081 "Itadakimasu API")
* [Itadakimasu API](http://192.168.160.223:8080/swagger-ui.html "Itadakimasu API")
* [Vroom^2 API](http://192.168.160.223:8081/swagger-ui.html "Vroom API")
* [SonarCloud](https://sonarcloud.io/organizations/fantastic7uni/projects "SonarCloud")


## Team
* Pedro Souto 93106 (Team Leader)
* Maria Inês 93320 (Product Owner)
* Fábio Carmelino 93406 (QA Engineer)
* Diogo Moreira 93127 (DevOps Master)
* Luís Pereira 93321 (DevOps Master)
* All (Developers)
